// connect motor controller pins to Arduino digital pins
// motor one
int enA = 3;
int in1 = 8;
int in2 = 9;
// motor two
int enB = 5;
int in3 = 10;
int in4 = 11;
int motor1Pin1 = in1; // pin 2 on L293D IC
int motor1Pin2 = in2; // pin 7 on L293D IC
int enable1Pin = enA; // pin 1 on L293D IC
int motor2Pin1 = in3; // pin 10 on L293D IC
int motor2Pin2 = in4; // pin 15 on L293D IC
int enable2Pin = enB;
int state;
int flag=0;        //makes sure that the serial only prints once the state
int stateStop=0;
void setup()
{
  // set all the motor control pins to outputs
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  Serial.begin(9600);

  //Serial2.begin(9600);
}
/*
void demoOne()
{
  // this function will run the motors in both directions at a fixed speed
  // turn on motor A
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  // set speed to 200 out of possible range 0~255
  digitalWrite(enA, 200);
  // turn on motor B
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  // set speed to 200 out of possible range 0~255
  digitalWrite(enB, 200);
  delay(2000);
  // now change motor directions
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);  
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH); 
  delay(2000);
  // now turn off motors
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);  
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
}
void demoTwo()
{
  // this function will run the motors across the range of possible speeds
  // note that maximum speed is determined by the motor itself and the operating voltage
  // the PWM values sent by analogWrite() are fractions of the maximum speed possible 
  // by your hardware
  // turn on motors
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);  
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH); 
  // accelerate from zero to maximum speed
  for (int i = 0; i &lt; 256; i++)
  {
    analogWrite(enA, i);
    analogWrite(enB, i);
    delay(20);
  } 
  // decelerate from maximum speed to zero
  for (int i = 255; i &gt;= 0; --i)
  {
    analogWrite(enA, i);
    analogWrite(enB, i);
    delay(20);
  } 
  // now turn off motors
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);  
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);  
}*/
void loop()
{
  //demoOne();
  //delay(1000);
 // demoTwo();
 // delay(1000);
 String input="";
 // if(Serial2.available() > 0){     
 //     input = Serial2.read();   
 //     flag=0;
  // }
 
  while(Serial.available()){
   input+=(char)Serial.read();
    //input = Serial2.read();
      
    // if the state is '1' the DC motor will go forward
    if (input == "B") {
        digitalWrite(motor1Pin1, HIGH);
        digitalWrite(motor1Pin2, LOW); 
        digitalWrite(motor2Pin1, HIGH);
        digitalWrite(motor2Pin2, LOW);
        
    }
    
    // if the state is '2' the motor will turn left
    else if (input == "L") {
        digitalWrite(motor1Pin1, LOW); 
        digitalWrite(motor1Pin2, HIGH); 
        digitalWrite(motor2Pin1, HIGH);
        digitalWrite(motor2Pin2, LOW);
    }
    // if the state is '3' the motor will Stop
    else if (input == "S") {
        digitalWrite(motor1Pin1, LOW); 
        digitalWrite(motor1Pin2, LOW); 
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, LOW);
      
    }
    // if the state is '4' the motor will turn right
    else if (input == "R") {
        digitalWrite(motor1Pin1, HIGH); 
        digitalWrite(motor1Pin2, LOW); 
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, HIGH);
      
    }
    // if the state is '5' the motor will Reverse
    else if (input == "F") {
        digitalWrite(motor1Pin1, LOW); 
        digitalWrite(motor1Pin2, HIGH);
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, HIGH);
       
    }
    Serial.println(input);
  }
} 
